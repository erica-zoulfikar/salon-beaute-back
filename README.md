# Salon Beauté Back

L'appli back-end du projet

## Installation

```node
npm install
```

## Usage

```js
```

## Dossiers

- **API Endpoints (Routes)**: dossier api/ contient les definitions des routes.
- **Configuration**: dossier config/ contient les fichier de configuration, notament la base de donnée.
- **Models**: dossier models/ contient les models mongoose pour MongoDB
- **Services**: dossier services/ contient toutes les fonctions qui ne sont pas directement liées à un model.
- **Middleware**: dossier middleware/ est utiliser pour les modules de expressJS, comme l'authentification ou les gestions d'erreurs
- **Utilities**: dossier utils/ contient les fonctions utilisées dans toute l'application (ex: calcul de date)
- **App Entry Point**: app.js est là où tout est liés, configuration de express, routes, middleware, etc

## License

[MIT](https://choosealicense.com/licenses/mit/)