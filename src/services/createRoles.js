const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const Role = require('../models/roleModel');
const User = require('../models/userModel');
const dbConfig = require('../config/db');

// Connectez-vous à la base de données
mongoose.connect(dbConfig.url);

// Créez les rôles initiaux
const roles = [
  { name: 'manager' },
  { name: 'employee' },
  { name: 'client' },
];

const createRoles = async (req, res, next) => {
  try {
    // Insérez les rôles dans la base de données
    const insertedRoles = await Role.insertMany(roles);

    // Récupérez l'ID du rôle "manager"
    const managerRoleId = insertedRoles.find(role => role.name === 'manager')._id;

    // Hachez le mot de passe
    const hashedPassword = await bcrypt.hash('password', 10);

    // Créez un utilisateur avec le rôle "manager" et le mot de passe haché
    await User.create({
      role: managerRoleId,
      email: 'ratsimisetamialyerica@gmail.com',
      password: hashedPassword,
      delete_flag: false,
    });

    res.status(200).json({ message: 'Succès lors de la création des rôles' });
  } catch (error) {
    next(error);
    res.status(500).json({ message: 'Erreur lors de la création des rôles' });
  } finally {
    // Déconnectez-vous de la base de données après l'opération
    mongoose.disconnect();
  }
}

module.exports = {
  createRoles
};
