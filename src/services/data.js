const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const dbConfig = require('../config/db');

// Importez vos modèles ici
const User = require('../models/userModel');
const Client = require('../models/clientModel');
const Employee = require('../models/employeeModel');
const Service = require('../models/serviceModel');
const Appointment = require('../models/appointmentModel');

mongoose.connect(dbConfig.url), {
  useNewUrlParser: true,
  useUnifiedTopology: true,
};

const  insertData = async (req, res, next) => {
  try {
    // Insertion de plusieurs utilisateurs
    const users = [
      { role: 'admin', email: 'admin@example.com', password: await bcrypt.hash('password123', 10) },
      { role: 'client', email: 'client1@example.com', password: await bcrypt.hash('password123', 10) },
      { role: 'employee', email: 'employee1@example.com', password: await bcrypt.hash('password123', 10) }
    ];
    const insertedUsers = await User.insertMany(users);

    // Insertion de plusieurs services
    const services = [
      { label: 'Coiffure', duration: 60, price: 50, commission: 10 },
      { label: 'Manucure', duration: 90, price: 75, commission: 15 }
    ];
    const insertedServices = await Service.insertMany(services);

    // Insertion de plusieurs employés
    const employees = [
      { id_user: insertedUsers[2]._id, username: 'Lova', gender: 'Homme', job: 'Coiffure', employment_date: new Date() },
      { id_user: insertedUsers[2]._id, username: 'Vola', gender: 'Femme', job: 'Onglerie', employment_date: new Date() }
    ];
    const insertedEmployees = await Employee.insertMany(employees);

    // Insertion de plusieurs clients
    const clients = [];
    for (let i = 0; i < 8; i++) {
      clients.push({ id_user: insertedUsers[1]._id, username: `Client ${i + 1}`, gender: i % 2 === 0 ? 'Femme' : 'Homme', number: `1234567${i}` });
    }
    const insertedClients = await Client.insertMany(clients);

    // Insertion de 10 rendez-vous avec des dates espacées
    const appointments = [];
    const currentDate = new Date();
    for (let i = 0; i < 10; i++) {
      const date = new Date(currentDate);
      date.setDate(currentDate.getDate() + i);
      const appointmentData = {
        id_service: insertedServices[i % 2]._id,
        id_employee: insertedEmployees[i % 2]._id,
        id_client: insertedClients[i]._id,
        date: date,
        status: 'confirmed',
        start: date,
        end: new Date(date.getTime() + (60 * 60 * 1000)), // Ajouter une heure
        text: `Rendez-vous ${i + 1}`
      };
      appointments.push(appointmentData);
    }
    await Appointment.insertMany(appointments);

    console.log('Données insérées avec succès!');
  } catch (error) {
    console.error('Erreur lors de l\'insertion des données :', error);
  } finally {
    mongoose.connection.close();
  }
}

module.exports = {
    insertData
};
  