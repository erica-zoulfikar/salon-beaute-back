const mongoose = require('mongoose');
const Appointment = require('../models/appointmentModel');
const Employee = require('../models/employeeModel');
const Service = require('../models/serviceModel');
// Fonction pour calculer le bénéfice par mois en tenant compte des dépenses
exports.getProfitPerMonth = async (req, res) => {
    try {
        const { dateDebut, dateFin, salaire = 0, loyer = 0, achatPiece = 0, autresDepenses = 0 } = req.query;

        // Vérifier si les paramètres nécessaires sont présents
        if (!dateDebut || !dateFin) {
            return res.status(400).json({ error: 'Paramètres manquants pour le calcul du bénéfice mensuel' });
        }

        const startOfMonth = new Date(dateDebut);
        startOfMonth.setHours(0, 0, 0, 0);

        const endOfMonth = new Date(dateFin);
        endOfMonth.setHours(23, 59, 59, 999);

        // Construire la requête pour récupérer les rendez-vous dans la plage de dates spécifiée
        const query = {
            date: { $gte: startOfMonth, $lte: endOfMonth },
            status: 'confirmed',
        };

        const appointments = await Appointment.find(query).populate('id_service');

        // Initialiser un objet pour stocker les profits par mois
        const profitsPerMonth = {};

        // Boucler à travers les rendez-vous pour calculer le chiffre d'affaires par mois
        appointments.forEach(appointment => {
            // Vérifier si le service associé existe
            if (appointment.id_service) {
                // Calculer le montant du service
                const serviceAmount = appointment.id_service.price;

                // Récupérer le mois de l'appointment
                const appointmentMonth = new Date(appointment.date).getMonth();

                // Initialiser le profit pour ce mois s'il n'existe pas encore
                if (!profitsPerMonth[appointmentMonth]) {
                    profitsPerMonth[appointmentMonth] = 0;
                }

                // Ajouter le montant du service au profit pour ce mois
                profitsPerMonth[appointmentMonth] += serviceAmount;
            }
        });

        // Boucler à travers les mois pour soustraire les dépenses et obtenir le profit par mois
        const profits = Object.keys(profitsPerMonth).map(month => {
            const monthDate = new Date(startOfMonth.getFullYear(), parseInt(month), 1);
            const monthName = new Intl.DateTimeFormat('fr-FR', { month: 'long' }).format(monthDate);

            const monthProfit = profitsPerMonth[month] - parseFloat(salaire) - parseFloat(loyer) - parseFloat(achatPiece) - parseFloat(autresDepenses);
            return {
                label: monthName,
                data: monthProfit
            };
        });

        // Retourner le résultat
        res.status(200).json(profits);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Erreur lors du calcul du bénéfice mensuel' });
    }
};


// Fonction pour calculer le temps moyen de travail pour chaque employé dans un mois ou une année spécifique
exports.getAverageWorkTimeEmp = async (req, res) => {
    try {
        const { date, interval } = req.query;

        let startOfInterval;
        let endOfInterval;

        // Vérifier si l'intervalle est 'jour' ou 'mois'
        if (interval === 'jour') {
            // Si l'intervalle est 'jour', extraire le mois de la date
            const dateObj = new Date(date);
            startOfInterval = new Date(dateObj.getFullYear(), dateObj.getMonth(), 1);
            endOfInterval = new Date(dateObj.getFullYear(), dateObj.getMonth() + 1, 0, 23, 59, 59, 999);
        } else if (interval === 'mois') {
            // Si l'intervalle est 'mois', extraire l'année de la date
            const dateObj = new Date(date);
            startOfInterval = new Date(dateObj.getFullYear(), 0, 1);
            endOfInterval = new Date(dateObj.getFullYear(), 11, 31, 23, 59, 59, 999);
        } else {
            throw new Error('L\'intervalle doit être spécifié comme "jour" ou "mois".');
        }

        const query = {
            date: { $gte: startOfInterval, $lte: endOfInterval },
            status: 'confirmed',
        };

        const appointments = await Appointment.find(query).populate('id_employee id_service');

        // Initialiser un objet pour stocker le temps de travail par employé
        const tempsTravailParEmploye = {};

        for (const appointment of appointments) {
            // Vérifier si l'employé associé et le service associé existent
            if (appointment.id_employee && appointment.id_service) {
                try {
                    // Chercher l'employé à partir de son id
                    const employee = await Employee.findById(appointment.id_employee);
                    
                    if (employee) {
                        // Récupérer le nom d'utilisateur de l'employé
                        const employeeUsername = employee.username;

                        // Calculer la durée du service
                        const serviceDuration = appointment.id_service.duration || 0;

                        // Ajouter la durée du service au temps de travail par employé
                        tempsTravailParEmploye[employeeUsername] = (tempsTravailParEmploye[employeeUsername] || 0) + serviceDuration;
                    }
                } catch (error) {
                    console.error(`Erreur lors de la recherche de l'employé : ${error.message}`);
                }
            }
        }

        // Calculer le temps moyen de travail par employé
        const tempsMoyenTravailParEmploye = {};

        for (const employeeUsername of Object.keys(tempsTravailParEmploye)) {
            const totalDuration = tempsTravailParEmploye[employeeUsername];
            
            // Filtrer les rendez-vous de l'employé actuel
            const employeeAppointments = appointments.filter(appointment => appointment.id_employee._id.toString() === appointment.id_employee._id.toString());

            const numberOfAppointments = employeeAppointments.length;

            // Calculer le temps moyen en minutes
            const tempsMoyen = numberOfAppointments > 0 ? totalDuration / numberOfAppointments : 0;

            tempsMoyenTravailParEmploye[employeeUsername] = tempsMoyen;
        }

        // Convertir l'objet en tableau d'objets avec la structure souhaitée
        const resultArray = Object.keys(tempsMoyenTravailParEmploye).map(employeeUsername => ({
            label: `${employeeUsername}`,
            data: tempsMoyenTravailParEmploye[employeeUsername]
        }));

        res.status(200).json(resultArray);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Erreur lors du calcul du temps moyen de travail par employé' });
    }
};



// Fonction pour calculer le chiffre d'affaires pour un mois donné
exports.getChiffreDaffaireMois = async (req, res) => {
    try {
        const { dateDebut, dateFin } = req.query;

        // Vérifier si les paramètres nécessaires sont présents
        if (!dateDebut || !dateFin) {
            return res.status(400).json({ error: 'Paramètres manquants pour le calcul du chiffre d\'affaires mensuel' });
        }

        const startOfMonth = new Date(dateDebut);
        startOfMonth.setHours(0, 0, 0, 0);

        const endOfMonth = new Date(dateFin);
        endOfMonth.setHours(23, 59, 59, 999);

        // Construire la requête pour récupérer les rendez-vous dans la plage de dates spécifiée
        const query = {
            date: { $gte: startOfMonth, $lte: endOfMonth },
            status: 'confirmed',
        };

        const appointments = await Appointment.find(query).populate('id_service');

        // Initialiser un objet pour stocker les chiffres d'affaires par mois
        const chiffreDaffaireParMois = {};

        appointments.forEach(appointment => {
            // Vérifier si le service associé existe
            if (appointment.id_service) {
                // Récupérer le mois de l'appointment
                const appointmentMonth = new Date(appointment.date).getMonth();

                // Ajouter le montant du service au chiffre d'affaires par mois
                chiffreDaffaireParMois[appointmentMonth] = (chiffreDaffaireParMois[appointmentMonth] || 0) + appointment.id_service.price;
            }
        });

        // Retourner un tableau de chiffres d'affaires par mois
        const result = {
            label: `Chiffre d'affaires par mois pour la période de ${startOfMonth.toLocaleDateString()} à ${endOfMonth.toLocaleDateString()}`,
            data: chiffreDaffaireParMois
        };
        res.status(200).json(result);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Erreur lors du calcul du chiffre d\'affaires mensuel' });
    }
};

// Fonction pour calculer le chiffre d'affaires par jour pour un mois donné
exports.getChiffreDaffaireJour = async (req, res) => {
    try {
        const { dateDebut, dateFin } = req.query;

        // Vérifier si les paramètres nécessaires sont présents
        if (!dateDebut || !dateFin) {
            return res.status(400).json({ error: 'Paramètres manquants pour le calcul du chiffre d\'affaires quotidien' });
        }

        const startOfMonth = new Date(dateDebut);
        startOfMonth.setHours(0, 0, 0, 0);

        const endOfMonth = new Date(dateFin);
        endOfMonth.setHours(23, 59, 59, 999);

        // Construire la requête pour récupérer les rendez-vous dans la plage de dates spécifiée
        const query = {
            date: { $gte: startOfMonth, $lte: endOfMonth },
            status: 'confirmed',
        };

        const appointments = await Appointment.find(query).populate('id_service');

        // Initialiser un objet pour stocker les chiffres d'affaires par jour
        const chiffreDaffaireParJour = {};

        appointments.forEach(appointment => {
            // Vérifier si le service associé existe
            if (appointment.id_service) {
                // Récupérer la date du rendez-vous
                const appointmentDate = appointment.date.toISOString().split('T')[0];

                // Ajouter le montant du service au chiffre d'affaires par jour
                chiffreDaffaireParJour[appointmentDate] = (chiffreDaffaireParJour[appointmentDate] || 0) + appointment.id_service.price;
            }
        });

        // Retourner un tableau de chiffres d'affaires par jour
        const result = {
            label: `Chiffre d'affaires par jour pour le mois de ${startOfMonth.toLocaleDateString()} à ${endOfMonth.toLocaleDateString()}`,
            data: chiffreDaffaireParJour
        };
        res.status(200).json(result);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Erreur lors du calcul du chiffre d\'affaires quotidien' });
    }
};



//Le nombre de réservation par jour
exports.getReservationsPerDay = async (req, res) => {
    try {
        const { dateDebut, dateFin } = req.query;

        // Vérifier si les paramètres nécessaires sont présents
        if (!dateDebut || !dateFin) {
            return res.status(400).json({ error: 'Paramètres manquants pour le calcul du nombre de réservations par jour' });
        }

        const startOfDay = new Date(dateDebut);
        startOfDay.setHours(0, 0, 0, 0);

        const endOfDay = new Date(dateFin);
        endOfDay.setHours(23, 59, 59, 999);

        // Construire la requête pour récupérer les rendez-vous dans la plage de dates spécifiée
        const query = {
            date: { $gte: startOfDay, $lte: endOfDay },
            status: 'confirmed',
        };

        const reservations = await Appointment.find(query);

        // Regrouper les réservations par jour
        const reservationsPerDay = {};

        reservations.forEach(appointment => {
            const appointmentDay = new Date(appointment.date).toLocaleDateString('en-US', { month: 'long', day: 'numeric', year: 'numeric' });

            if (!reservationsPerDay[appointmentDay]) {
                reservationsPerDay[appointmentDay] = 1;
            } else {
                reservationsPerDay[appointmentDay]++;
            }
        });

        // Convertir l'objet en tableau d'objets avec la structure souhaitée
        const resultArray = Object.keys(reservationsPerDay).map(day => ({
            label: `${day}`,
            data: reservationsPerDay[day]
        }));

        res.status(200).json(resultArray);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Erreur lors du calcul du nombre de réservations par jour' });
    }
};

//Le nombre de réservation par mois
exports.getReservationsPerMonth = async (req, res) => {
    try {
        const { dateDebut, dateFin } = req.query;

        // Vérifier si les paramètres nécessaires sont présents
        if (!dateDebut || !dateFin) {
            return res.status(400).json({ error: 'Paramètres manquants pour le calcul du nombre de réservations par mois' });
        }

        const startOfMonth = new Date(dateDebut);
        startOfMonth.setDate(1); // Premier jour du mois

        const endOfMonth = new Date(dateFin);
        endOfMonth.setMonth(endOfMonth.getMonth() + 1, 0); // Dernier jour du mois

        // Construire la requête pour récupérer les rendez-vous dans la plage de dates spécifiée
        const query = {
            date: { $gte: startOfMonth, $lte: endOfMonth },
            status: 'confirmed',
        };

        const reservations = await Appointment.find(query);

        // Regrouper les réservations par mois
        const reservationsPerMonth = {};

        reservations.forEach(appointment => {
            const appointmentMonth = new Date(appointment.date).toLocaleDateString('en-US', { month: 'long', year: 'numeric' });

            if (!reservationsPerMonth[appointmentMonth]) {
                reservationsPerMonth[appointmentMonth] = 1;
            } else {
                reservationsPerMonth[appointmentMonth]++;
            }
        });

        // Convertir l'objet en tableau d'objets avec la structure souhaitée
        const resultArray = Object.keys(reservationsPerMonth).map(month => ({
            label: `${month}`,
            data: reservationsPerMonth[month]
        }));

        res.status(200).json(resultArray);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Erreur lors du calcul du nombre de réservations par mois' });
    }
};
