const User = require('../models/userModel');
const Role = require('../models/roleModel');

const getAllUser = async (req, res) => {
  try {
    const users = await User.find({ delete_flag: false });
    res.json(users);
  } catch (error) {
    res.status(500).json({ message: 'Une erreur s\'est produite lors de la récupération des employés' });
  }
};

const getUserById = async (req, res) => {
  const id = req.params.id;

  try {
    const user = await User.findOne({ _id: id, delete_flag: false });
    if (!user) return res.status(404).json({ message: 'Utilisateur non trouvé' });

    res.json(user);
  } catch (error) {
    res.status(500).json({ message: 'Une erreur s\'est produite lors de la récupération de l\'employé' });
  }
};

module.exports = {
  getAllUser,
  getUserById,
};
