const User = require("../models/userModel");
const Client = require("../models/clientModel");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const Employee = require("../models/employeeModel");
const JWT_SECRET  = process.env.JWT_SECRET;

const register = async (req, res) => {
  const { role, email, password } = req.body;

  try {
    const existingUser = await User.findOne({ email });
    if (existingUser)
      return res.status(400).json({ message: "L'utilisateur existe déjà" });

    const hashedPassword = await bcrypt.hash(password, 10);
    await User.create({
      role,
      email,
      password: hashedPassword,
      delete_flag: false,
    });

    res.status(201).json({ message: "Inscription réussie" });
  } catch (error) {
    error.message = "Une erreur s'est produite lors de l'inscription"
    next(error)
  }
};

const login = async (req, res, next) => {
  try {
    const { email, password } = req.body;

    // Vérification si l'utilisateur existe
    var user = await User.findOne({ email });
    if (!user) {
      return res.status(401).json({ message: "Identifiants invalides." });
    }

    // Vérification du mot de passe
    const passwordMatch = await bcrypt.compare(password, user.password);
    if (!passwordMatch) {
      return res.status(401).json({ message: "Identifiants invalides." });
    }

    // Création des tokens d'accès et de rafraîchissement
    const token = jwt.sign(
      { userId: user._id, role: user.role }, process.env.JWT_SECRET, { expiresIn: "1d" }
    );
    
    if (user.role === 'employee') {
      const employee = await Employee.findOne({ id_user: user._id })
      .populate('id_user');
      if (employee) {
        user = { 
          ...employee._doc.id_user._doc,
          ...employee._doc,
          id_user: employee._doc.id_user._doc._id,
          _id: employee._id
        };
      }
    } else if (user.role === 'client') {
      const client = await Client.findOne({ id_user: user._id })
      .populate('preferences.services')
      .populate('preferences.employees')
      .populate({
        path: 'preferences.employees',
        populate: {
          path: 'id_user',
          model: 'User' // Ensure this matches the name you used when calling mongoose.model for your User model
        }
      })
      .populate('id_user');
      if (client) {
        user = { 
          ...client._doc.id_user._doc,
          ...client._doc,
          id_user: client._doc.id_user._doc._id,
          _id: client._id 
        };
      }
    }
    
    res.json({ token: token, user: user });
  } catch (error) {
    next(error);
  }
};

const reloadUser = async (req, res, next) => {
  try {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];

    const decoded = jwt.verify(token, JWT_SECRET);

    var user = await User.findOne({ _id: decoded.userId })
    if (user.role === 'employee') {
      const employee = await Employee.findOne({ id_user: user._id })
      .populate('id_user');
      if (employee) {
        user = { ...employee._doc.id_user._doc, ...employee._doc, id_user: employee._doc.id_user._doc._id, _id: employee._id };
      }
    } else if (user.role === 'client') {
      const client = await Client.findOne({ id_user: user._id })
      .populate('preferences.services')
      .populate('preferences.employees')
      .populate({
        path: 'preferences.employees',
        populate: {
          path: 'id_user',
          model: 'User' // Ensure this matches the name you used when calling mongoose.model for your User model
        }
      })
      .populate('id_user');
      if (client) {
        let employeesPreference = client.preferences.employees.map((employee) => {
          return {
            ...employee.id_user._doc,
            ...employee._doc, 
            id_user: employee.id_user._id.toString(),
            _id: employee._id 
          };
        });
        user = { 
          ...client._doc.id_user._doc,
          ...client._doc,
          id_user: client._doc.id_user._doc._id,
          _id: client._id,
          preferences: { employees: employeesPreference, services: client._doc.preferences.services }
        }
      }
    }
    
    res.json(user)
    next();
  } catch (err) {
    if (err instanceof jwt.JsonWebTokenError) {
      err.status = 403;
      err.message = 'Token d\'accès invalide';
    } else {
      err.status = err.status || 500;
      err.message = err.message || 'Internal Server Error';
    }
    next(err);
  }
}

module.exports = {
  register,
  login,
  reloadUser
};
