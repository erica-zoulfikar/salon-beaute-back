const Client = require('../models/clientModel');
const User = require('../models/userModel');
const Role = require('../models/roleModel');
const bcrypt = require('bcrypt');

const getAllClients = async (req, res) => {
  try {
    const clients = await Client.find({ delete_flag: false }).populate('id_user');
    res.json(clients);
  } catch (error) {
    res.status(500).json({ message: 'Une erreur s\'est produite lors de la récupération des clients' });
  }
};

const getClientById = async (req, res) => {
  const id = req.params.id;

  try {
    const client = await Client.findOne({ _id: id, delete_flag: false }).populate('id_user');
    if (!client) return res.status(404).json({ message: 'Client non trouvé' });

    res.json(client);
  } catch (error) {
    res.status(500).json({ message: 'Une erreur s\'est produite lors de la récupération du client' });
  }
};

const createClient = async (req, res) => {
  const session = await Client.startSession();
  session.startTransaction();

  try {
    const hashedPassword = await bcrypt.hash(req.body.password, 10);
    const user = await User.create({
      role: req.body.role,
      email: req.body.email,
      password: hashedPassword,
    });

    const client = await Client.create({
      id_user: user._id,
      username: req.body.username,
      gender: req.body.gender,
      number: req.body.number,
      preferences: {
        employees: [],
        services: [],
      },
      delete_flag: false,
    });

    const role = await Role.findOne({ name: req.body.role });
    if (!role) {
      throw new Error('Le rôle spécifié n\'existe pas.');
    }

    user.role = role.name;
    await user.save();

    await session.commitTransaction();
    session.endSession();

    res.status(201).json({
      message: 'Client et utilisateur créés avec succès',
      data: client,
    });
  } catch (error) {
    await session.abortTransaction();
    session.endSession();

    res.status(500).json({
      message: 'Une erreur s\'est produite lors de la création du client et de l\'utilisateur: ' + error.message,
    });
  }
};

const updateClient = async (req, res) => {
  const id = req.params.id;
  const { username, gender, number, preferences } = req.body;

  try {
    const client = await Client.findByIdAndUpdate(
      id,
      { username, gender, number, preferences },
      { new: true }
    ).populate('id_user');

    if (!client) return res.status(404).json({ message: 'Client non trouvé' });

    res.json({ message: 'Client mis à jour avec succès', data: client });
  } catch (error) {
    res.status(500).json({ message: 'Une erreur s\'est produite lors de la mise à jour du client' });
  }
};

const deleteClient = async (req, res) => {
  const id = req.params.id;

  try {
    const client = await Client.findByIdAndUpdate(
      id,
      { delete_flag: true },
      { new: true }
    ).populate('id_user');

    if (!client) return res.status(404).json({ message: 'Client non trouvé' });

    // Marquer l'utilisateur associé comme supprimé
    await User.findByIdAndUpdate(client.id_user, { delete_flag: true });

    res.json({ message: 'Client et utilisateur marqués comme supprimés avec succès', data: client });
  } catch (error) {
    res.status(500).json({ message: 'Une erreur s\'est produite lors de la suppression du client' });
  }
};


const addPreference = async (req, res) => {
  const userId = req.params.userId; // Utiliser l'ID de l'utilisateur connecté
  const { employees, services } = req.body;

  try {
    const client = await Client.findOne({ id_user: userId, delete_flag: false });
    
    if (!client) {
      return res.status(404).json({ message: 'Client non trouvé' });
    }

    // Ajouter de nouvelles préférences aux tableaux existants
    if (employees && employees.length > 0) {
      client.preferences.employees.push(...employees);
    }

    if (services && services.length > 0) {
      client.preferences.services.push(...services);
    }

    await client.save();

    res.json({ message: 'Préférences ajoutées avec succès', data: client });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Une erreur s\'est produite lors de l\'ajout des préférences' });
  }
};

const updatePreference = async (req, res) => {
  const userId = req.params.userId; // Utiliser l'ID de l'utilisateur connecté
  const { employees, services } = req.body.preferences;

  try {
    const client = await Client.findOneAndUpdate(
      { id_user: userId, delete_flag: false },
      {
        $addToSet: {
          'preferences.employees': { $each: employees },
          'preferences.services': { $each: services },
        },
      },
      { new: true }
    ).populate('id_user');

    if (!client) {
      return res.status(404).json({ message: 'Client non trouvé' });
    }

    res.json({ message: 'Préférences mises à jour avec succès', data: client });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Une erreur s\'est produite lors de la mise à jour des préférences' });
  }
};

const removePreference = async (req, res) => {
  const userId = req.params.userId;
  const { preferenceType, preferenceId } = req.body;

  try {
    const client = await Client.findOne({ id_user: userId, delete_flag: false });

    if (!client) {
      return res.status(404).json({ message: 'Client non trouvé' });
    }

    // Supprimer la préférence du tableau correspondant
    if (preferenceType === 'employees') {
      client.preferences.employees = client.preferences.employees.filter(emp => emp.toString() !== preferenceId);
    } else if (preferenceType === 'services') {
      client.preferences.services = client.preferences.services.filter(serv => serv.toString() !== preferenceId);
    } else {
      return res.status(400).json({ message: 'Type de préférence non valide' });
    }

    await client.save();

    res.json({ message: 'Préférence supprimée avec succès', data: client });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Une erreur s\'est produite lors de la suppression de la préférence' });
  }
};

module.exports = {
  getAllClients,
  getClientById,
  createClient,
  updateClient,
  deleteClient,
  addPreference,
  updatePreference,
  removePreference,
};
