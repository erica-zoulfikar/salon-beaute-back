const SpecialOffer = require('../models/specialOfferModel');

// Créer une offre spéciale
exports.createSpecialOffer = async (req, res) => {
  try {
    const specialOffer = new SpecialOffer(req.body);
    await specialOffer.save();
    res.status(201).json(specialOffer);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// Lire toutes les offres spéciales
exports.getSpecialOffers = async (req, res) => {
  try {
    const specialOffers = await SpecialOffer.find({ delete_flag: false });
    res.json(specialOffers);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// Lire une offre spéciale par son ID
exports.getSpecialOfferById = async (req, res) => {
  try {
    const specialOffer = await SpecialOffer.findById(req.params.id);
    if (!specialOffer || specialOffer.delete_flag) {
      return res.status(404).json({ message: 'Special Offer not found' });
    }
    res.json(specialOffer);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// Mettre à jour une offre spéciale
exports.updateSpecialOffer = async (req, res) => {
  try {
    const specialOffer = await SpecialOffer.findByIdAndUpdate(req.params.id, req.body, { new: true });
    if (!specialOffer) {
      return res.status(404).json({ message: 'Special Offer not found' });
    }
    res.json(specialOffer);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// Supprimer une offre spéciale (logiquement en définissant delete_flag sur true)
exports.deleteSpecialOffer = async (req, res) => {
  try {
    const specialOffer = await SpecialOffer.findByIdAndUpdate(req.params.id, { delete_flag: true }, { new: true });
    if (!specialOffer) {
      return res.status(404).json({ message: 'Special Offer not found' });
    }
    res.json({ message: 'Special Offer marked as deleted' });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};
