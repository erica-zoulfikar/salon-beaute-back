const Notification = require('../models/notificationModel');

exports.createNotification = async (req, res) => {
    try {
        const { id_user, date, message } = req.body;
        const notification = new Notification({ id_user, date, message });
        await notification.save();
        res.status(201).json(notification);
    } catch (error) {
        console.error('Error creating notification:', error);
        res.status(500).json({ error: 'Failed to create notification' });
    }
};

exports.getAllNotifications = async (req, res) => {
    try {
        const notifications = await Notification.find();
        res.json(notifications);
    } catch (error) {
        console.error('Error getting notifications:', error);
        res.status(500).json({ error: 'Failed to get notifications' });
    }
};

exports.getUpcomingUnreadNotifications = async (req, res) => {
    try {
        const userId = req.params.userId;

        const today = new Date();
        today.setHours(0, 0, 0, 0);

        const twoDaysFromNow = new Date(today);
        twoDaysFromNow.setDate(today.getDate() + 2);
        twoDaysFromNow.setHours(23, 59, 59, 999);

        const notifications = await Notification.find({
            id_user: userId,
            date: { $gte: today, $lte: twoDaysFromNow },
            status: 'unread'
        });

        res.json(notifications);
    } catch (error) {
        console.error('Error getting upcoming unread notifications:', error);
        res.status(500).json({ error: 'Failed to get upcoming unread notifications' });
    }
};

exports.updateNotification = async (req, res) => {
    try {
        const notificationId = req.params.id;

        const updatedNotification = await Notification.findByIdAndUpdate(
            notificationId,
            { $set: { status: 'read' } },
            { new: true }
        );

        if (!updatedNotification) {
            return res.status(404).json({ error: 'Notification not found' });
        }

        res.json(updatedNotification);
    } catch (error) {
        console.error('Error updating notification:', error);
        res.status(500).json({ error: 'Failed to update notification' });
    }
};
