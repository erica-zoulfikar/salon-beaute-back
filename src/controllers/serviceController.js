const Service = require('../models/serviceModel');

const multer = require('multer'); // Module pour gérer les fichiers uploadés
const path = require('path');

// Configuration de Multer pour l'upload d'images
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/'); // Dossier où les images seront stockées
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
    const fileExtension = path.extname(file.originalname).toLowerCase();
    cb(null, 'image-' + uniqueSuffix + fileExtension); // Nom du fichier
  }
});

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 5120 * 1024 // Taille maximale autorisée (5 Mo)
  },
  fileFilter: function (req, file, cb) {
    const allowedExtensions = ['.jpg', '.jpeg', '.png', '.gif', '.mov'];
    const fileExtension = path.extname(file.originalname).toLowerCase();
    if (allowedExtensions.includes(fileExtension)) {
      cb(null, true); // Accepter le fichier
    } else {
      cb(new Error('Format de fichier non pris en charge.')); // Refuser le fichier
    }
  }
});

// Créer un service avec upload d'image
exports.createService = async (req, res) => {
  try {
    // Utilisez `upload.single('image')` pour gérer l'upload d'une seule image
    // Le champ 'image' doit correspondre au nom du champ dans votre formulaire
    upload.single('image')(req, res, async function (err) {
      if (err) {
        return res.status(400).json({ message: err.message });
      }
      // Le fichier a été correctement uploadé, vous pouvez maintenant enregistrer les autres données du service
      const service = new Service({
        label: req.body.label,
        duration: req.body.duration,
        description: req.body.description,
        price: req.body.price,
        commission: req.body.commission
      });
      await service.save();
      res.status(201).json(service);
    });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// Créer un service
// exports.createService = async (req, res) => {
//   try {
//     const service = new Service(req.body);
//     await service.save();
//     res.status(201).json(service);
//   } catch (error) {
//     res.status(500).json({ message: error.message });
//   }
// };

// Lire tous les services
exports.getServices = async (req, res) => {
  try {
    const services = await Service.find({ delete_flag: false }); // Ne récupère que les services non supprimés
    res.json(services);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// Lire un service par son ID
exports.getServiceById = async (req, res) => {
  try {
    const service = await Service.findById(req.params.id);
    if (!service || service.delete_flag) {
      return res.status(404).json({ message: 'Service not found' });
    }
    res.json(service);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// Mettre à jour un service
exports.updateService = async (req, res) => {
  try {
    const service = await Service.findByIdAndUpdate(req.params.id, req.body, { new: true });
    if (!service) {
      return res.status(404).json({ message: 'Service not found' });
    }
    res.json(service);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// Supprimer un service (logiquement en définissant delete_flag sur true)
exports.deleteService = async (req, res) => {
  try {
    const service = await Service.findByIdAndUpdate(req.params.id, { delete_flag: true }, { new: true });
    if (!service) {
      return res.status(404).json({ message: 'Service not found' });
    }
    res.json({ message: 'Service marked as deleted' });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }

  function calculateDiscountPrice(originalPrice, discountPercentage) {
    // Assuming discount is a percentage of the original price
    return originalPrice - (originalPrice * (discountPercentage / 100));
  }

  exports.getServiceWithOffers = async (req, res) => {
    app.get('/services-with-offers', async (req, res) => {
    try {
      const today = new Date();
      const services = await Service.find({ delete_flag: false });
      const servicesWithOffers = await Promise.all(services.map(async (service) => {
        const specialOffer = await SpecialOffer.findOne({
          id_service: service._id,
          start_date: { $lte: today },
          end_date: { $gte: today },
          delete_flag: false
        });
  
        // If there's an active special offer, adjust the price
        if (specialOffer) {
          const discountPrice = calculateDiscountPrice(service.price, specialOffer.discount);
          return { ...service.toObject(), price: discountPrice, specialOffer: specialOffer.description };
        }
  
        return service.toObject();
      }));

      res.json(servicesWithOffers);
    } catch (error) {
      console.error('Failed to fetch services with offers:', error);
      res.status(500).send('Internal server error');
    }
    });
  }
};
