const Bill = require('../models/billModel');

// Créer une facture
exports.createBill = async (req, res) => {
  try {
    const bill = new Bill(req.body);
    await bill.save();
    res.status(201).json(bill);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// Lire toutes les factures
exports.getBills = async (req, res) => {
  try {
    const bills = await Bill.find({ delete_flag: false });
    res.json(bills);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// Lire une facture par son ID
exports.getBillById = async (req, res) => {
  try {
    const bill = await Bill.findById(req.params.id);
    if (!bill || bill.delete_flag) {
      return res.status(404).json({ message: 'Bill not found' });
    }
    res.json(bill);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// Mettre à jour une facture
exports.updateBill = async (req, res) => {
  try {
    const bill = await Bill.findByIdAndUpdate(req.params.id, req.body, { new: true });
    if (!bill) {
      return res.status(404).json({ message: 'Bill not found' });
    }
    res.json(bill);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// Supprimer une facture (logiquement en définissant delete_flag sur true)
exports.deleteBill = async (req, res) => {
  try {
    const bill = await Bill.findByIdAndUpdate(req.params.id, { delete_flag: true }, { new: true });
    if (!bill) {
      return res.status(404).json({ message: 'Bill not found' });
    }
    res.json({ message: 'Bill marked as deleted' });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};
