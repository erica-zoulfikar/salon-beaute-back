const Appointment = require('../models/appointmentModel');
const User = require('../models/userModel');
const Notification = require('../models/notificationModel');
const mongoose = require('mongoose');

function formatDate(date) {
    const d = new Date(date);
    const day = d.getDate().toString().padStart(2, '0');
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    const month = months[d.getMonth()];
    const year = d.getFullYear();
    return `${day}/${month}/${year}`;
}

function formatTime(date) {
    const hours = date.getHours().toString().padStart(2, '0');
    const minutes = date.getMinutes().toString().padStart(2, '0');
    return `${hours}:${minutes}`;
  }

exports.createAppointment = async (req, res) => {
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
        const { id_service, id_employee, date, start, end, text } = req.body;

        // Validation des champs obligatoires
        if (!id_service || !id_employee || !date || !start || !end || !text) {
            return res.status(400).json({ error: 'Champs obligatoires manquants pour la création du rendez-vous' });
        }

        // Récupérer l'ID de l'utilisateur connecté à partir des informations d'authentification
        const id_client = req.params.userId;

        // Vérifier si la date du rendez-vous est dans le futur
        const currentDateTime = new Date();
        if (new Date(date) < currentDateTime) {
            return res.status(400).json({ error: 'La date du rendez-vous doit être dans le futur' });
        }

        // Vérifier si un rendez-vous existe déjà à la même date et à la même heure pour le même employé
        const existingAppointment = await Appointment.find({
            id_employee,
            id_client,
            status: { $in: ['pending', 'confirmed', 'restored'] },
            date: {
              $gte: start,
              $lte: end
            }
          });

        if (existingAppointment.length > 0) {
            console.log(existingAppointment)
            return res.status(400).json({ error: 'Un rendez-vous existe déjà à cette date et heure pour cet employé' });
        }

        // Créer un nouveau rendez-vous
        const newAppointment = new Appointment({
            id_service,
            id_employee,
            id_client,
            date,
            start,
            end,
            text
        });

        // Enregistrer le rendez-vous dans la base de données
        await newAppointment.save();

        // Créer une nouvelle notification
        const notificationMessage = `Nouveau rendez-vous prévu pour ${formatDate(new Date(date))} à ${formatTime(new Date(start))}`;
        const notification = new Notification({
            id_user: id_client,
            date: new Date(),
            status: 'unread',
            message: notificationMessage
        });

        // Enregistrer la notification dans la base de données
        await notification.save();

        await session.commitTransaction();
        session.endSession();

        res.status(201).json({ message: 'Rendez-vous créé avec succès', id: newAppointment._id.toString() });
    } catch (error) {
        await session.abortTransaction();
        session.endSession();
        console.error(error);
        res.status(500).json({ error: 'Erreur lors de la création du rendez-vous et de la notification' });
    }
};

exports.updateAppointment = async (req, res) => {
    try {
        const appointmentId = req.params.userId; // Assuming the appointment ID is passed as a URL parameter
        const { id_service, id_employee, date, start, end, text } = req.body;
        console.log(req.body)

        // Fetch the existing appointment
        const appointment = await Appointment.findById(appointmentId);

        if (!appointment) {
            return res.status(404).json({ error: 'Rendez-vous non trouvé' });
        }

        // Validation des champs obligatoires
        if (!id_service || !id_employee || !date || !start || !end || !text) {
            return res.status(400).json({ error: 'Champs obligatoires manquants pour la mise à jour du rendez-vous' });
        }

        // Vérifier si la date du rendez-vous est dans le futur
        const currentDateTime = new Date();
        if (new Date(date) < currentDateTime) {
            return res.status(400).json({ error: 'La date du rendez-vous doit être dans le futur' });
        }

        // Vérifier si un rendez-vous existe déjà à la même date et à la même heure pour le même employé
        const overlappingAppointment = await Appointment.findOne({
            _id: { $ne: appointmentId }, // Exclude the current appointment from the check
            id_employee,
            status: { $in: ['pending', 'confirmed', 'restored'] },
            date: {
              $gte: start,
              $lte: end
            }
        });

        if (overlappingAppointment) {
            return res.status(400).json({ error: 'Un rendez-vous existe déjà à cette date et heure pour cet employé' });
        }

        // Update the appointment fields
        appointment.id_service = id_service;
        appointment.id_employee = id_employee;
        appointment.date = date;
        appointment.start = `${start}Z`;
        appointment.end = `${end}Z`;
        appointment.text = text;

        // Save the updated appointment
        await appointment.save();

        res.status(200).json({ message: 'Rendez-vous mis à jour avec succès', id: appointment._id.toString() });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Erreur lors de la mise à jour du rendez-vous' });
    }
};

exports.getAppointmentHistory = async (req, res) => {
    try {
        const userId = req.params.userId;

        const page = parseInt(req.query.page) || 1;
        const pageSize = 5;
        const skip = (page - 1) * pageSize;

        const query = {
            $and: [
                { $or: [{ id_employee: userId }, { id_client: userId }] },
                { $or: [{ status: 'confirmed' }, { status: 'restored' }] },
            ]
        };
        

        const appointmentHistory = await Appointment.find(query).populate('id_service').populate('id_employee').populate('id_client')
            .sort({ date: -1 })
            .skip(skip)
            .limit(pageSize);

        res.status(200).json(appointmentHistory);
    
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Erreur lors de la récupération de l\'historique des rendez-vous' });
    }
};

exports.getPendingAppointment = async (req, res) => {
    try {
        const userId = req.params.userId;
        const startDate = req.query.startDate ? new Date(req.query.startDate) : null;
        const endDate = req.query.endDate ? new Date(req.query.endDate) : null;

        const page = parseInt(req.query.page) || 1;
        const pageSize = 5;
        const skip = (page - 1) * pageSize;

        const startOfDay = new Date(startDate);
        startOfDay.setHours(0, 0, 0, 0); // Set the time to the beginning of the day

        const endOfDay = new Date(endDate);
        endOfDay.setHours(23, 59, 59, 999); // Set the time to the end of the day

        const query = {
            $and: [
                { $or: [{ id_employee: userId }, { id_client: userId }] },
                { $or: [{ status: 'pending' }] },
                startDate && endDate ? { date: { $gte: startOfDay, $lte: endOfDay } } : {}
            ]
        };


        const appointmentHistory = await Appointment.find(query).populate('id_service').populate('id_employee').populate('id_client')
            .sort({ date: -1 })
            .skip(skip)
            .limit(pageSize);

        res.status(200).json(appointmentHistory);
    
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Erreur lors de la récupération de l\'historique des rendez-vous en attente' });
    }
};

exports.getPendingAppointmentWoDate = async (req, res) => {
    try {
        const userId = req.params.userId;

        const query = {
            $and: [
                { $or: [{ id_employee: userId }, { id_client: userId }] },
                { $or: [{ status: 'pending' }] },
            ]
        };

        const appointmentHistory = await Appointment.find(query).populate('id_service').populate('id_employee').populate('id_client')
            .sort({ date: -1 })
            .skip(skip)
            .limit(pageSize);

        res.status(200).json(appointmentHistory);
    
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Erreur lors de la récupération de l\'historique des rendez-vous en attente' });
    }
};

// Contrôleur pour confirmer un rendez-vous
exports.confirmAppointment = async (req, res) => {
    try {
        const appointmentId = req.params.appointmentId;

        // Récupérer le rendez-vous à partir de la base de données
        const appointment = await Appointment.findById(appointmentId);

        if (!appointment) {
            return res.status(404).json({ error: 'Rendez-vous non trouvé' });
        }

        // Vérifier si le rendez-vous est en attente de confirmation
        if (appointment.status === 'pending') {
            // Mettre à jour le statut du rendez-vous à "confirmed"
            appointment.status = 'confirmed';
            await appointment.save();

            res.status(200).json({ message: 'Rendez-vous confirmé avec succès' });
        } else {
            res.status(400).json({ error: 'Le rendez-vous ne peut pas être confirmé car il n\'est pas en attente' });
        }
    } catch (error) {
        res.status(500).json({ error: 'Erreur lors de la confirmation du rendez-vous' });
    }
};

// Contrôleur pour annuler un rendez-vous
exports.deleteAppointment = async (req, res) => {
    try {
        const appointmentId = req.params.appointmentId;

        const appointment = await Appointment.findById(appointmentId);

        if (!appointment) {
            return res.status(404).json({ error: 'Rendez-vous non trouvé' });
        }

        if (appointment.status === 'pending' || appointment.status === 'confirmed') {
            await Appointment.deleteOne({ _id: appointmentId });

            res.status(200).json({ message: 'Rendez-vous supprimé avec succès' });
        } else {
            res.status(400).json({ error: 'Le rendez-vous ne peut pas être supprimé car il n\'est ni en attente ni confirmé' });
        }
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Erreur lors de la suppression du rendez-vous' });
    }
};


// Contrôleur pour annuler un rendez-vous
exports.cancelAppointment = async (req, res) => {
    try {
        const appointmentId = req.params.appointmentId;

        // Récupérer le rendez-vous à partir de la base de données
        const appointment = await Appointment.findById(appointmentId);

        if (!appointment) {
            return res.status(404).json({ error: 'Rendez-vous non trouvé' });
        }

        // Vérifier si le rendez-vous est en attente de confirmation ou déjà confirmé
        if (appointment.status === 'pending' || appointment.status === 'confirmed') {
            // Mettre à jour le statut du rendez-vous à "cancelled"
            appointment.status = 'cancelled';
            await appointment.save();

            res.status(200).json({ message: 'Rendez-vous annulé avec succès' });
        } else {
            res.status(400).json({ error: 'Le rendez-vous ne peut pas être annulé car il n\'est ni en attente ni confirmé' });
        }
    } catch (error) {
        res.status(500).json({ error: 'Erreur lors de l\'annulation du rendez-vous' });
    }
};

// Contrôleur pour restaurer un rendez-vous annulé
exports.restoreAppointment = async (req, res) => {
    try {
        const appointmentId = req.params.appointmentId;

        // Récupérer le rendez-vous à partir de la base de données
        const appointment = await Appointment.findById(appointmentId);

        if (!appointment) {
            return res.status(404).json({ error: 'Rendez-vous non trouvé' });
        }

        // Vérifier si le rendez-vous est annulé
        if (appointment.status === 'cancelled') {
            // Mettre à jour le statut du rendez-vous à "restored"
            appointment.status = 'restored';
            await appointment.save();

            res.status(200).json({ message: 'Rendez-vous restauré avec succès' });
        } else {
            res.status(400).json({ error: 'Le rendez-vous ne peut pas être restauré car il n\'est pas annulé' });
        }
    } catch (error) {
        res.status(500).json({ error: 'Erreur lors de la restauration du rendez-vous' });
    }
};

exports.getAllRdv = async (req, res) => {
    try {
        const { dateDebut, dateFin, serviceId, employeeId } = req.query;
        const userId = req.params.userId; // Utiliser l'ID de l'utilisateur connecté

        const page = parseInt(req.query.page) || 1;
        const pageSize = 5;
        const skip = (page - 1) * pageSize;

        // Créer un objet pour construire la requête
        const query = { $or: [{ id_employee: userId }, { id_client: userId }] };

        // Ajouter les filtres optionnels à la requête
        if (serviceId) {
            query.id_service = serviceId;
        }

        if (employeeId) {
            query.id_employee = employeeId;
        }

        // Ajouter les filtres de date à la requête s'ils sont spécifiés
        if (dateDebut && dateFin) {
            const startOfDay = new Date(dateDebut);
            startOfDay.setHours(0, 0, 0, 0);

            const endOfDay = new Date(dateFin);
            endOfDay.setHours(23, 59, 59, 999);

            query.date = { $gte: startOfDay, $lte: endOfDay };
        }

        // Récupérer tous les rendez-vous de la base de données spécifiques à la requête
        const appointmentHistory = await Appointment.find(query)
            .sort({ date: -1 })
            .skip(skip)
            .limit(pageSize);

        res.status(200).json(appointmentHistory);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Erreur lors de la récupération des rendez-vous' });
    }
};

exports.searchAppointments = async (req, res) => {
    try {
        const { dateDebut, dateFin, serviceId, employeeId } = req.query;
        const userId = req.params.userId; // Utiliser l'ID de l'utilisateur connecté

        const page = parseInt(req.query.page) || 1;
        const pageSize = 5;
        const skip = (page - 1) * pageSize;

        // Construire la requête de recherche
        const query = {
            $or: [{ id_employee: userId }, { id_client: userId }],
        };

        // Ajouter les filtres optionnels à la requête
        if (serviceId) {
            query.id_service = serviceId;
        }

        if (employeeId) {
            query.id_employee = employeeId;
        }

        // Ajouter les filtres de date à la requête s'ils sont spécifiés
        if (dateDebut && dateFin) {
            const startOfDay = new Date(dateDebut);
            startOfDay.setHours(0, 0, 0, 0);

            const endOfDay = new Date(dateFin);
            endOfDay.setHours(23, 59, 59, 999);

            query.date = { $gte: startOfDay, $lte: endOfDay };
        }

        // Récupérer tous les rendez-vous de la base de données spécifiques à la requête
        const appointmentResults = await Appointment.find(query)
            .sort({ date: -1 })
            .skip(skip)
            .limit(pageSize);

        res.status(200).json(appointmentResults);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Erreur lors de la recherche des rendez-vous' });
    }
};
