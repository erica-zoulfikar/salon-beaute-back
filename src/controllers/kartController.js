const Kart = require('../models/kartModel');

// Contrôleur pour créer un kart
exports.createKart = async (req, res) => {
    try {
        const newKart = new Kart();
        await newKart.save();

        res.status(201).json({ message: 'Kart créé avec succès', kartId: newKart._id });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Erreur lors de la création du kart' });
    }
};

// Contrôleur pour ajouter un rendez-vous à un kart
exports.addToKart = async (req, res) => {
    try {
        const kartId = req.params.kartId;
        const appointmentId = req.body.appointmentId;

        // Vérifier si le kart existe
        const kart = await Kart.findById(kartId);
        if (!kart) {
            return res.status(404).json({ error: 'Kart non trouvé' });
        }

        // Ajouter le rendez-vous au kart
        kart.appointment.push(appointmentId);
        await kart.save();

        res.status(200).json({ message: 'Rendez-vous ajouté au kart avec succès' });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Erreur lors de l\'ajout du rendez-vous au kart' });
    }
};

// Contrôleur pour récupérer les rendez-vous d'un kart
exports.getKartAppointments = async (req, res) => {
    try {
        const kartId = req.params.kartId;

        // Vérifier si le kart existe
        const kart = await Kart.findById(kartId).populate('appointment');
        if (!kart) {
            return res.status(404).json({ error: 'Kart non trouvé' });
        }

        res.status(200).json(kart.appointment);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Erreur lors de la récupération des rendez-vous du kart' });
    }
};


// Contrôleur pour mettre à jour un kart update du status
exports.updateKart = async (req, res) => {
    try {
        const kartId = req.params.kartId;
        const { status } = req.body;

        // Vérifier si le kart existe
        const kart = await Kart.findById(kartId);
        if (!kart) {
            return res.status(404).json({ error: 'Kart non trouvé' });
        }

        // Mettre à jour les informations du kart
        if (status) {
            kart.status = status;
        }

        // Enregistrez les modifications dans la base de données
        await kart.save();

        res.status(200).json({ message: 'Kart mis à jour avec succès' });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Erreur lors de la mise à jour du kart' });
    }
};

