const Employee = require('../models/employeeModel');
const User = require('../models/userModel');
const Role = require('../models/roleModel');
const Horraire = require('../models/horraireModel');
const bcrypt = require('bcrypt');
const Appointment = require('../models/appointmentModel');
const Service = require('../models/serviceModel');


const getAllEmployees = async (req, res) => {
  try {
    const employees = await Employee.find({ delete_flag: false }).populate('id_user');
    res.json(employees);
  } catch (error) {
    res.status(500).json({ message: 'Une erreur s\'est produite lors de la récupération des employés' });
  }
};

const getEmployeeById = async (req, res) => {
  const id = req.params.id;

  try {
    const employee = await Employee.findOne({ _id: id, delete_flag: false }).populate('id_user');
    if (!employee) return res.status(404).json({ message: 'Employé non trouvé' });

    res.json(employee);
  } catch (error) {
    res.status(500).json({ message: 'Une erreur s\'est produite lors de la récupération de l\'employé' });
  }
};

const createEmployee = async (req, res) => {
  // Début de la transaction
  const session = await Employee.startSession();
  session.startTransaction();

  try {
    // Créer un nouvel utilisateur
    const hashedPassword = await bcrypt.hash(req.body.password, 10);
    const user = await User.create({
      role: req.body.role,
      email: req.body.email,
      password: hashedPassword,
    });

    // Créer un nouvel employé
    const employee = await Employee.create({
      id_user: user._id,
      username: req.body.username,  // Utiliser le nom d'utilisateur fourni dans la requête
      gender: req.body.gender,
      job: req.body.job,
      employment_date: req.body.employment_date,
      delete_flag: false,
    });

    // Assigner le rôle à l'utilisateur
    const role = await Role.findOne({ name: req.body.role });
    if (!role) {
      throw new Error('Le rôle spécifié n\'existe pas.');
    }

    user.role = role.name;
    await user.save();

    // Valider la transaction
    await session.commitTransaction();
    session.endSession();

    res.status(201).json({
      message: 'Employé et utilisateur créés avec succès',
      data: employee,
    });
  } catch (error) {
    await session.abortTransaction();
    session.endSession();

    res.status(500).json({
      message: 'Une erreur s\'est produite lors de la création de l\'employé et de l\'utilisateur: ' + error.message,
    });
  }
};


const updateEmployee = async (req, res) => {
  const id = req.params.id;
  const { username, gender, job, employment_date } = req.body;

  try {
    const employee = await Employee.findByIdAndUpdate(
      id,
      { username, gender, job, employment_date },
      { new: true }
    ).populate('id_user');

    if (!employee) return res.status(404).json({ message: 'Employé non trouvé' });

    res.json({ message: 'Employé mis à jour avec succès', data: employee });
  } catch (error) {
    res.status(500).json({ message: 'Une erreur s\'est produite lors de la mise à jour de l\'employé' });
  }
};


const deleteEmployee = async (req, res) => {
  const id = req.params.id;

  try {
    const employee = await Employee.findByIdAndUpdate(
      id,
      { delete_flag: true },
      { new: true }
    ).populate('id_user');

    if (!employee) return res.status(404).json({ message: 'Employé non trouvé' });

    // Marquer l'utilisateur associé comme supprimé
    await User.findByIdAndUpdate(employee.id_user, { delete_flag: true });

    res.json({ message: 'Employé et utilisateur marqués comme supprimés avec succès', data: employee });
  } catch (error) {
    res.status(500).json({ message: 'Une erreur s\'est produite lors de la suppression de l\'employé' });
  }
};

const createHorraire = async (req, res) => {
  try {
    const id_employee = req.params.id;  // Utilisez l'ID de l'employé connecté
    const { start_time, end_time } = req.body;

    const newHorraire = await Horraire.create({
      id_employee,
      start_time,
      end_time,
    });

    res.status(201).json({ message: 'Horaire de travail créé avec succès', data: newHorraire });
  } catch (error) {
    res.status(500).json({ message: 'Une erreur s\'est produite lors de la création de l\'horaire de travail' });
  }
};
// Obtenir les horaires de travail pour l'employé connecté entre deux dates
const getHorraires = async (req, res) => {
  try {
    const id_employee = req.params.id;  // Utilisez l'ID de l'employé connecté
    const { startDate, endDate, page } = req.query;

    const pageSize = 5;
    const skip = (parseInt(page) - 1) * pageSize;

    let query = { id_employee };

    // Si startDate est fourni, ajouter la condition $gte
    if (startDate) {
      query.start_time = { $gte: new Date(startDate) };
    }

    // Si endDate est fourni, ajouter la condition $lte
    if (endDate) {
      query.end_time = { $lte: new Date(endDate) };
    }

    const horraires = await Horraire.find(query)
      .sort({ end_time: -1 })  // Triez par end_time de la plus récente à la moins récente
      .skip(skip)
      .limit(pageSize);

    res.json(horraires);
  } catch (error) {
    res.status(500).json({ message: 'Une erreur s\'est produite lors de la récupération des horaires de travail' });
  }
};



// Mettre à jour un horaire de travail pour l'employé connecté
const updateHorraire = async (req, res) => {
  const horraireId = req.params.horraireId;
  const { start_time, end_time } = req.body;

  try {
    const updatedHorraire = await Horraire.findOneAndUpdate(
      { _id: horraireId, id_employee: req.params.id },  // Assurez-vous que l'horraire appartient à l'employé connecté
      { start_time, end_time },
      { new: true }
    );

    if (!updatedHorraire) {
      return res.status(404).json({ message: 'Horaire de travail non trouvé' });
    }

    res.json({ message: 'Horaire de travail mis à jour avec succès', data: updatedHorraire });
  } catch (error) {
    res.status(500).json({ message: 'Une erreur s\'est produite lors de la mise à jour de l\'horaire de travail' });
  }
};

// Supprimer un horaire de travail pour l'employé connecté
const deleteHorraire = async (req, res) => {
  const horraireId = req.params.horraireId;

  try {
    const deletedHorraire = await Horraire.findOneAndDelete({ _id: horraireId, id_employee: req.params.id });

    if (!deletedHorraire) {
      return res.status(404).json({ message: 'Horaire de travail non trouvé' });
    }

    res.json({ message: 'Horaire de travail supprimé avec succès' });
  } catch (error) {
    res.status(500).json({ message: 'Une erreur s\'est produite lors de la suppression de l\'horaire de travail' });
  }
};

const getCommission = async (req, res) => {
  try {
    const userId = req.params.userId; // Utiliser l'ID de l'employé connecté
    let { startDate, endDate } = req.query;

    // Vérifier si startDate est spécifié, sinon utiliser la date d'aujourd'hui
    startDate = startDate ? new Date(startDate) : new Date();

    // Vérifier si endDate est spécifié, sinon utiliser la date d'aujourd'hui
    endDate = endDate ? new Date(endDate) : new Date();

    // Ajuster les dates pour inclure la fin de la journée
    endDate.setHours(23, 59, 59, 999);

    // Récupérer tous les rendez-vous de l'employé connecté dans la période spécifiée
    const appointments = await Appointment.find({
      $and: [
        { $or: [{ id_employee: userId }] },
        { date: { $gte: startDate, $lte: endDate } },
        { status: 'confirmed' }, // Vous pouvez ajuster en fonction de votre logique de statut
      ],
    });

    // Calculer la commission totale
    let totalCommission = 0;
    for (const appointment of appointments) {
      try {
        const service = await Service.findById(appointment.id_service);
        // Vérifier si le service a été trouvé
        if (service && service.commission !== undefined && service.price !== undefined) {
          const commissionPercentage = service.commission;
          const commissionAmount = service.price * (commissionPercentage / 100);
          totalCommission += commissionAmount;
        } else {
          console.error('Service not found or missing commission/price information');
        }
      } catch (error) {
        console.error('Error while fetching service:', error.message);
      }
    }

    res.status(200).json({ totalCommission });
  } catch (error) {
    console.error('Erreur lors de la récupération de la commission des rendez-vous', error);
    res.status(500).json({ error: 'Une erreur s\'est produite lors de la récupération de la commission des rendez-vous' });
  }
};


module.exports = {
  getAllEmployees,
  getEmployeeById,
  createEmployee,
  updateEmployee,
  deleteEmployee,
  createHorraire,
  getHorraires,
  updateHorraire,
  deleteHorraire,
  getCommission, 
};