exports.validateServiceData = (req, res, next) => {
  const { label, description, price } = req.body; // Changer de name à label et de description à desc
  if (!label || !description || !price) {
    return res.status(400).json({ message: "Missing required fields" });
  }
  next();
};
