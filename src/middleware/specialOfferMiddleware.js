exports.validateSpecialOfferData = (req, res, next) => {
    const { id_service, start_date, end_date, description } = req.body;
  
    // Assurez-vous que les champs requis sont présents
    if (!id_service || !start_date || !end_date || !description) {
      return res.status(400).json({ message: "Missing required fields" });
    }
  
    next();
  };
  