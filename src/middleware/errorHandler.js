const logger = require('../config/logger');

module.exports = ((err, req, res, next) => {
    logger.error(`${err.status || 500} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);
    console.error(err)
    if (err.status == 500) {
      res.status(500).send('Erreur Serveur');
    } else {
      res.status(err.status).send(err.message);
    }
  });
  