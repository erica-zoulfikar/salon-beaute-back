const jwt = require('jsonwebtoken');
const config = require('../config/logger');

module.exports = async (req, res, next) => {
  const token = req.headers.authorization.split(' ')[1];

  try {
    const decoded = jwt.verify(token, config.secret);
    req.userData = decoded;

    // Ajoutez ici des vérifications supplémentaires si nécessaire (par exemple, le rôle de l'utilisateur)

    next();
  } catch (error) {
    return res.status(401).json({ message: 'Authentification échouée' });
  }
};
