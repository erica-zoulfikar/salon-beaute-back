exports.validateBillData = (req, res, next) => {
    const { label, price, start_date } = req.body;
  
    if (!label || !price || !start_date) {
      return res.status(400).json({ message: "Missing required fields" });
    }
    next();
  };
  