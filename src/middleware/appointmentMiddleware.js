const mongoose = require('mongoose');
const Appointment = require('../models/appointmentModel');

// Middleware pour valider l'ID du rendez-vous
exports.validateAppointmentId = async (req, res, next) => {
    try {
        const { appointmentId } = req.params;

        // Vérifier si l'ID du rendez-vous est valide
        if (!mongoose.Types.ObjectId.isValid(appointmentId)) {
            return res.status(400).json({ error: 'ID de rendez-vous invalide' });
        }

        // Rechercher le rendez-vous dans la base de données
        const appointment = await Appointment.findById(appointmentId);

        // Vérifier si le rendez-vous existe
        if (!appointment) {
            return res.status(404).json({ error: 'Rendez-vous non trouvé' });
        }

        // Ajouter le rendez-vous à la demande
        req.appointment = appointment;

        // Passer à la prochaine fonction
        next();
    } catch (error) {
        next(error);
    }
};
