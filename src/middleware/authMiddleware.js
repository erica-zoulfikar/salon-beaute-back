const jwt = require('jsonwebtoken');
const JWT_SECRET  = process.env.JWT_SECRET;


const authenticateToken = async (req, res, next) => {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];
  
  if (!token) {
    const err = new Error('Token d\'accès manquant');
    err.status = 401;
    return next(err);
  }
  
  jwt.verify(token, JWT_SECRET, (err, user) => {
    if (err) {
      err.status = 403;
      err.message = 'Token d\'accès invalide';
      return next(err);
    }
    next();
  });
};


module.exports = {
  authenticateToken,
};
