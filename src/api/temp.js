const express = require('express');
const router = express.Router();
const services = require('../services/createRoles');
const data = require('../services/data');


router.get('/createRoles', services.createRoles)
router.get('/insert/data', data.insertData)


module.exports = router;
