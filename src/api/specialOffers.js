const express = require('express');
const router = express.Router();
const specialOfferController = require('../controllers/specialOfferController');
const { validateSpecialOfferData } = require('../middleware/specialOfferMiddleware');

// Middleware de validation des données pour la création
router.post('/', validateSpecialOfferData, specialOfferController.createSpecialOffer);
// Autres routes sans middleware de validation
router.get('/', specialOfferController.getSpecialOffers);
router.get('/:id', specialOfferController.getSpecialOfferById);
router.put('/:id', specialOfferController.updateSpecialOffer);
router.delete('/:id', specialOfferController.deleteSpecialOffer);

module.exports = router;
