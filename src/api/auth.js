const express = require('express');
const router = express.Router();
const authController = require('../controllers/authController');
const { authenticateToken } = require('../middleware/authMiddleware');

router.post('/register', authController.register);
router.post('/login', authController.login);
router.get('/reloadUser', authenticateToken, authController.reloadUser);
router.get('/verifyToken', authenticateToken, (req, res, next) => {
  try {
    res.json({message: 'Token Valide'})
  } catch (err) {
    next(err)
  }
});
module.exports = router;
