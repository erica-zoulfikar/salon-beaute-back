const express = require('express');
const router = express.Router();
const serviceController = require('../controllers/serviceController');
// const { validateServiceData } = require('../middleware/validationMiddleware');

//Routes pour les services
// Middleware de validation des données
router.post('/', serviceController.createService);
router.put('/:id', serviceController.updateService);
router.get('/', serviceController.getServices);
router.get('/:id', serviceController.getServiceById);
router.delete('/:id', serviceController.deleteService);

module.exports = router;
