const express = require('express');
const router = express.Router();
const notificationController = require('../controllers/notificationController');
const { authenticateToken } = require('../middleware/authMiddleware');


// Route pour créer une notification
router.post('/', notificationController.createNotification);

// Route pour obtenir toutes les notifications
router.get('/', notificationController.getAllNotifications);

// Route pour obtenir les notifications à venir non lues
router.get('/upcoming-unread/:userId', authenticateToken, notificationController.getUpcomingUnreadNotifications);

// Route pour marquer une notification comme lue
router.put('/read/:id', authenticateToken, notificationController.updateNotification);

module.exports = router;
