const express = require('express');
const router = express.Router();
const clientController = require('../controllers/clientController');
const { authenticateToken } = require('../middleware/authMiddleware');

router.get('/', clientController.getAllClients);
router.get('/:id', clientController.getClientById);
router.post('/', clientController.createClient);
router.put('/:id', clientController.updateClient);
router.delete('/:id', clientController.deleteClient);
router.post('/add-preference/:userId', authenticateToken, clientController.addPreference);
router.put('/update-preference/:userId', authenticateToken, clientController.updatePreference);
router.delete('/remove-preference/:userId', authenticateToken, clientController.removePreference);


module.exports = router;
