const express = require('express');
const router = express.Router();
const billController = require('../controllers/billController');
const { validateBillData } = require('../middleware/billMiddleware');

// Middleware de validation des données pour la création et la mise à jour
router.post('/', validateBillData, billController.createBill);
router.put('/:id', billController.updateBill);
router.get('/', billController.getBills);
router.get('/:id', billController.getBillById);
router.delete('/:id', billController.deleteBill);

module.exports = router;
