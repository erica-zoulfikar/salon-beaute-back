const express = require('express');
const statisticsController = require('../controllers/statisticsController');

const router = express.Router();

router.get('/chiffredaffaire/jour', statisticsController.getChiffreDaffaireJour);
router.get('/chiffredaffaire/mois', statisticsController.getChiffreDaffaireMois);
router.get('/reservationsPerDay', statisticsController.getReservationsPerDay);
router.get('/reservationsPerMonth', statisticsController.getReservationsPerMonth);
router.get('/averageWorkTimeEmp', statisticsController.getAverageWorkTimeEmp);
router.get('/profitpermonth', statisticsController.getProfitPerMonth);

module.exports = router;
