const express = require('express');
const router = express.Router();
const employeeController = require('../controllers/employeeController');

router.get('/', employeeController.getAllEmployees);
router.get('/:id', employeeController.getEmployeeById);
router.post('/', employeeController.createEmployee);
router.put('/:id', employeeController.updateEmployee);
router.delete('/:id', employeeController.deleteEmployee);


// Routes pour la gestion des horaires de travail
router.post('/:id/horaires', employeeController.createHorraire);
router.get('/:id/horaires', employeeController.getHorraires);
router.put('/:id/horaires/:horraireId', employeeController.updateHorraire);
router.delete('/:id/horaires/:horraireId', employeeController.deleteHorraire);

router.get('/commission/:userId', employeeController.getCommission); 
module.exports = router; 