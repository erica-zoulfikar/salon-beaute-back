const express = require('express');
const router = express.Router();
const kartController = require('../controllers/kartController');


// Créer un kart
router.post('/create', kartController.createKart);

// Ajouter un rendez-vous à un kart
router.post('/:kartId/add', kartController.addToKart);

// Récupérer les rendez-vous d'un kart
router.get('/:kartId/appointments', kartController.getKartAppointments);

// Mettre à jour un kart
router.put('/:kartId/update', kartController.updateKart);
module.exports = router;
