const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const authMiddleware = require('../middleware/userMiddleware');

// Inscription
router.get('/', userController.getAllUser);

// Connexion
router.get('/:id', userController.getUserById);

module.exports = router;
