const express = require('express');
const router = express.Router();
const appointmentController = require('../controllers/appointmentController');
const { authenticateToken } = require('../middleware/authMiddleware');

// Route pour créer un rendez-vous
router.post('/create/:userId', authenticateToken, appointmentController.createAppointment);

// Route pour créer un rendez-vous
router.put('/update/:userId', authenticateToken, appointmentController.updateAppointment);

// Route pour confirmer un rendez-vous
router.put('/:appointmentId/confirm', appointmentController.confirmAppointment);

// Route pour annuler un rendez-vous
router.put('/:appointmentId/cancel', appointmentController.cancelAppointment);

// Route pour annuler un rendez-vous
router.delete('/:appointmentId', appointmentController.deleteAppointment);

// Route pour restaurer un rendez-vous
router.put('/:appointmentId/restore', appointmentController.restoreAppointment);

// Route pour obtenir l'historique des rendez-vous
// router.get('/history', appointmentController.getAppointmentHistory);
router.get('/history/:userId', authenticateToken, appointmentController.getAppointmentHistory);

router.get('/pendingWoDate/:userId', authenticateToken, appointmentController.getPendingAppointmentWoDate);

router.get('/pending/:userId', authenticateToken, appointmentController.getPendingAppointment);


// Ajouter la route pour obtenir tous les rendez-vous entre deux dates avec des filtres optionnels
router.get('/all/:userId', authenticateToken, appointmentController.getAllRdv);

router.get('/search/:userId', appointmentController.searchAppointments);



module.exports = router;
