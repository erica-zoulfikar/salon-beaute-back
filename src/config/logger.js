const { createLogger, format, transports } = require('winston');
const { combine, timestamp, printf } = format;

const myFormat = printf(({ level, message, timestamp }) => {
  return `${timestamp} ${level}: ${message}`;
});

const logger = createLogger({
  format: combine(
    timestamp(),
    myFormat
  ),
  transports: [
    // Fichier log pour les erreurs
    new transports.File({ filename: 'errors.log', level: 'error' }),
  ],
});

module.exports = logger;