const connection = process.env.DB_CONNECTION_STRING;

module.exports = {
    url: connection
  };
  