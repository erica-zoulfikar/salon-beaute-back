require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const dbConfig = require('./config/db');
const cors = require('cors');
const errorHandler = require('./middleware/errorHandler');
const bodyParser = require('body-parser');
const authRoutes = require('./api/auth'); 
const employeeRoutes = require('./api/employees');
const clientRoutes = require('./api/clients');
const serviceRoutes = require('./api/services');
const userRoutes = require('./api/users');
const specialOfferRoutes = require('./api/specialOffers');
const billRoutes = require('./api/bills');
const appointmentRoutes = require('./api/appointments');
const kartRoutes = require('./api/karts');
const notificationRoutes = require('./api/notifications');
const statisticRoutes = require('./api/statistics');
const tempRoutes = require('./api/temp');

const app = express();

mongoose.connect(dbConfig.url);

app.use(cors({
  origin: ['http://localhost:4200', 'https://salon-beaute-front-qaaw.onrender.com']
}));

app.use(bodyParser.json());

// Utilisation des routes d'authentification pour l'inscription et la connexion
app.use('/api/auth', authRoutes);

// Utilisation des routes pour les clients
app.use('/api/clients', clientRoutes);

// Utilisation des routes pour les employés
app.use('/api/employees', employeeRoutes);

// Utilisation des routes pour les utilisateurs
app.use('/api/users', userRoutes);

// Utilisation des routes pour les services
app.use('/api/services', serviceRoutes);

// Utilisation des routes pour les offres spéciales
app.use('/api/specialoffers', specialOfferRoutes);

// Utilisation des routes pour les factures
app.use('/api/bills', billRoutes);

// Utilisation des routes pour les rendez-vous
app.use('/api/appointments', appointmentRoutes);

app.use('/api/karts', kartRoutes);

app.use('/api/notifications', notificationRoutes);

app.use('/api/statistics', statisticRoutes);

// test routes
app.use('/api/temp', tempRoutes);

// Middleware pour la gestion des erreurs
app.use(errorHandler);

const PORT = process.env.PORT;
app.listen(PORT, () => {
  console.log(`Server is running on localhost:${PORT}/`);
});
