const mongoose = require('mongoose');

const billSchema = new mongoose.Schema({
  label: { type: String, required: true },
  price: { type: Number, required: true },
  start_date: { type: Date, required: true },
  delete_flag: { type: Boolean, default: false }
});

module.exports = mongoose.model('Bill', billSchema);
