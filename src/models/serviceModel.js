const mongoose = require('mongoose');

const serviceSchema = new mongoose.Schema({
  label: { type: String, required: true },
  duration: { type: Number, required: true }, // Durée en minutes
  description: { type: String },
  price: { type: Number, required: true },
  commission: { type: Number, required: true }, // Commission en pourcentage
  image: { type: String },// Chemin vers l'image ou URL de l'image
  delete_flag: { type: Boolean, default: false } // Par défaut, le service n'est pas supprimé
});

module.exports = mongoose.model('Service', serviceSchema);
