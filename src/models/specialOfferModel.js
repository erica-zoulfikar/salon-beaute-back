// specialOfferModel.js
const mongoose = require('mongoose');

const specialOfferSchema = new mongoose.Schema({
  id_service: { type: mongoose.Schema.Types.ObjectId, ref: 'Service', required: true },
  start_date: { type: Date, required: true },
  end_date: { type: Date, required: true },
  description: { type: String }, // Nouveau champ description
  discount: { type: Number, required: true },
  delete_flag: { type: Boolean, default: false }
});

module.exports = mongoose.model('SpecialOffer', specialOfferSchema);
