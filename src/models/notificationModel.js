const mongoose = require('mongoose');

const notificationSchema = new mongoose.Schema({
    id_user: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
    date: { type: Date, required: true },
    status: { type: String, enum: ['read', 'unread'], default: 'unread' },
    message: { type: String, required: true }
});

module.exports = mongoose.model('Notification', notificationSchema);
