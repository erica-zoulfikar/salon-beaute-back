const mongoose = require('mongoose');

const clientSchema = new mongoose.Schema({
  id_user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  username: String,
  gender: {
    type: String,
    enum: ['Homme', 'Femme'],
  },
  number: String,
  preferences: {
    employees: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Employee' }],
    services: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Service' }],
  },
  delete_flag: { type: Boolean, default: false },
});

const Client = mongoose.model('Client', clientSchema);
module.exports = Client;
