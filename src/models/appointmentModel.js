const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Définition du schéma pour le rendez-vous
const appointmentSchema = new Schema({
    id_service: { type: Schema.Types.ObjectId, ref: 'Service' }, // Référence à un service
    id_employee: { type: Schema.Types.ObjectId, ref: 'Employee' }, // Référence à un employé
    id_client: { type: Schema.Types.ObjectId, ref: 'Client' }, // Référence à un client
    date: { type: Date, required: true },
    status: { type: String, enum: ['pending', 'confirmed', 'cancelled', 'restored'], default: 'pending' },
    start: { type: Date, required: true }, // Heure début rendez-vous
    end: { type: Date, required: true }, // Heure fin rendez-vous
    text: { type: String, required: true },
    delete_flag: { type: Boolean, default: false } // Drapeau de suppression
});

// Création du modèle Appointment à partir du schéma
const Appointment = mongoose.model('Appointment', appointmentSchema);

module.exports = Appointment;

