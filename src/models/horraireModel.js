const mongoose = require('mongoose');

const horraireSchema = new mongoose.Schema({
  id_employee: { type: mongoose.Schema.Types.ObjectId, ref: 'Employee' },
  start_time: { type: Date, required: true },
  end_time: { type: Date, required: true },
});

const Horraire = mongoose.model('Horraire', horraireSchema);
module.exports = Horraire;
