const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  role: String,
  email: {
    type: String,
    unique: true,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  delete_flag: { type: Boolean, default: false },
});

const User = mongoose.model('User', userSchema);
module.exports = User;
