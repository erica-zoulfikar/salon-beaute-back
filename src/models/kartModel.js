const mongoose = require('mongoose');

const kartSchema = new mongoose.Schema({
    appointment: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Appointment', // Référence au modèle des rendez-vous
        },
    ],
    status: {
        type: String,
        enum: ['active', 'inactive'], // Vous pouvez ajuster les statuts selon vos besoins
        default: 'active',
    },
    delete_flag: {
        type: Boolean,
        default: false,
    },
});

const Kart = mongoose.model('Kart', kartSchema);

module.exports = Kart;
