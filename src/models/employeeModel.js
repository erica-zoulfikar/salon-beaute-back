const mongoose = require('mongoose');

const employeeSchema = new mongoose.Schema({
  id_user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  username: String,  
  gender: {
    type: String,
    enum: ['Homme', 'Femme'],
  },
  job: String,
  employment_date: Date,
  delete_flag: { type: Boolean, default: false },
});

const Employee = mongoose.model('Employee', employeeSchema);
module.exports = Employee;
